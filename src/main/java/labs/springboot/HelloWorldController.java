package labs.springboot;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;


@Controller
@EnableAutoConfiguration
public class HelloWorldController {
	
	static String bgColor = "#e74c3c"; //red
	static {
        double rand = Math.random();
        if ( Math.random() < 0.3 ) {
		    bgColor = "#16a085"; //green
		} else if ( (Math.random() > 0.3) && (Math.random() < 0.5) ) {
		    bgColor = "#2980b9"; //blue
		} else if ( (Math.random() > 0.5) &&  (Math.random() < 0.8) ) {
			bgColor = "#be2edd"; //pink
		}
	}	

	@RequestMapping("/hello")
	@ResponseBody
	public String sayHello(HttpServletRequest request) {
    	System.out.println("Chosen color: " + bgColor);
        return "<body bgcolor='" + bgColor + "'>Greetings from a sad world !!! </h1></body>";
	}


}